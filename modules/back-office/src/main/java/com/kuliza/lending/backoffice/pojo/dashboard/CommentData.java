package com.kuliza.lending.backoffice.pojo.dashboard;

import org.flowable.engine.task.Comment;

import com.kuliza.lending.backoffice.exceptions.CommentIsNullException;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

public class CommentData {

	private String id;
	private String message;
	private String date;
	private String userId;

	public CommentData() {
		super();
	}

	public CommentData(String id, String message, String date, String userId) {
		super();
		this.id = id;
		this.message = message;
		this.date = date;
		this.userId = userId;
	}

	public CommentData(Comment comment) {
		super();
		if (comment != null) {
			this.id = comment.getId();
			this.message = comment.getFullMessage();
			this.date = CommonHelperFunctions.getDateInFormat(comment.getTime(), "dd-MM-yyyy HH:mm:ss");
			this.userId = comment.getUserId();
		} else {
			throw new CommentIsNullException("Comment could not be saved!");
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
