package com.kuliza.lending.backoffice.pojo.dashboard.config;

import com.kuliza.lending.backoffice.models.Variables;

/***
 * This is configuration class used for getting variables configuration
 * 
 * @author kuliza-330
 *
 */
public class AppVariablesConfig {

	private String key;
	private String label;

	public AppVariablesConfig(Variables variable) {
		super();
		this.label = variable.getLabel();
		this.key = variable.getMapping();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
