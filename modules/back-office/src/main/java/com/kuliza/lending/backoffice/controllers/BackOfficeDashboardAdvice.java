package com.kuliza.lending.backoffice.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.backoffice.exceptions.CommentIsNullException;
import com.kuliza.lending.backoffice.exceptions.CommentsNotEnabledForRoleException;
import com.kuliza.lending.backoffice.exceptions.DashboardNotConfiguredForRoleException;
import com.kuliza.lending.backoffice.exceptions.InvalidApplicationIdException;
import com.kuliza.lending.backoffice.exceptions.InvalidOutcomeException;
import com.kuliza.lending.backoffice.exceptions.RoleNotAvailableException;
import com.kuliza.lending.backoffice.exceptions.TabNotConfiguredForRoleException;
import com.kuliza.lending.backoffice.exceptions.UserNotInvolvedInApplicationException;
import com.kuliza.lending.backoffice.exceptions.VariableNotFoundException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

@ControllerAdvice
@RestController
public class BackOfficeDashboardAdvice {

	@ExceptionHandler(CommentIsNullException.class)
	public ResponseEntity<Object> commentIsNullException(final CommentIsNullException e) {
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
	}

	@ExceptionHandler(UserNotInvolvedInApplicationException.class)
	public ResponseEntity<Object> userNotInvolvedInApplication(final UserNotInvolvedInApplicationException e) {
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage()));
	}

	@ExceptionHandler(RoleNotAvailableException.class)
	public ResponseEntity<Object> roleNotAvailableException(final RoleNotAvailableException e) {
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
	}

	@ExceptionHandler(InvalidOutcomeException.class)
	public ResponseEntity<Object> invalidOutcomeException(final InvalidOutcomeException e) {
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage()));
	}

	@ExceptionHandler(DashboardNotConfiguredForRoleException.class)
	public ResponseEntity<Object> dashboardNotConfiguredForRoleException(
			final DashboardNotConfiguredForRoleException e) {
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.EXPECTATION_FAILED, e.getMessage()));
	}

	@ExceptionHandler(InvalidApplicationIdException.class)
	public ResponseEntity<Object> invalidApplicationIdException(final InvalidApplicationIdException e) {
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage()));
	}

	@ExceptionHandler(TabNotConfiguredForRoleException.class)
	public ResponseEntity<Object> tabNotConfiguredForRoleException(final TabNotConfiguredForRoleException e) {
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.EXPECTATION_FAILED, e.getMessage()));
	}

	@ExceptionHandler(VariableNotFoundException.class)
	public ResponseEntity<Object> variableNotFoundException(final VariableNotFoundException e) {
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage()));
	}

	@ExceptionHandler(CommentsNotEnabledForRoleException.class)
	public ResponseEntity<Object> commentNotEnabledForRoleException(final CommentsNotEnabledForRoleException e) {
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage()));
	}

}
