package com.kuliza.lending.backoffice.pojo.dashboard.config;

import com.kuliza.lending.backoffice.models.Tabs;

public class TabsConfig {

	private String id;
	private String label;

	public TabsConfig() {
		super();
	}

	public TabsConfig(String id, String label) {
		super();
		this.id = id;
		this.label = label;
	}

	public TabsConfig(Tabs tab) {
		super();
		this.id = tab.getTabKey();
		this.label = tab.getName();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
