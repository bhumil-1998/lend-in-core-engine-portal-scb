package com.kuliza.lending.backoffice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_cards_variables_mapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "variable_id", "card_id" }) })
public class CardsVariablesMapping extends BaseModel {

	@Column(nullable = false)
	private int cardVariableOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "card_id", nullable = false)
	private Cards card;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "variable_id")
	private Variables variable;

	public CardsVariablesMapping() {
		super();
		this.setIsDeleted(false);
	}

	public CardsVariablesMapping(int cardVariableOrder, Cards card, Variables variable) {
		super();
		this.cardVariableOrder = cardVariableOrder;
		this.card = card;
		this.variable = variable;
		this.setIsDeleted(false);
	}

	public int getCardVariableOrder() {
		return cardVariableOrder;
	}

	public void setCardVariableOrder(int cardVariableOrder) {
		this.cardVariableOrder = cardVariableOrder;
	}

	public Cards getCard() {
		return card;
	}

	public void setCard(Cards card) {
		this.card = card;
	}

	public Variables getVariable() {
		return variable;
	}

	public void setVariable(Variables variable) {
		this.variable = variable;
	}

}
