package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ProductDeploymentDao extends CrudRepository<ProductDeployment, Long> {

	public ProductDeployment findById(long id);

	public ProductDeployment findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<ProductDeployment> findByProductId(long productId);

	public List<ProductDeployment> findByProductIdAndIsDeleted(long productId, boolean isDeleted);

	public ProductDeployment findByIdentifier(String identifier);

	public ProductDeployment findByIdentifierAndStatusAndIsDeleted(String identifier, boolean status,
                                                                   boolean isDeleted);

	public ProductDeployment findByIdentifierAndUserIdAndStatusAndIsDeleted(String identifier, long userId,
                                                                            boolean status, boolean isDeleted);

}
