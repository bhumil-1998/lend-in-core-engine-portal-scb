package com.kuliza.lending.journey.customer_dashboard.pojo;

public class CustomerDashboardApplicationApproved {
	String securityCode;
	String loanAmount;
	String emi;
	String tenure;
	String loanContractNumber;
	String disbursementDate;
	String disbursementMethod;
	String applicationDate;
	String applicationStatus;
	String loanContractUrl;

	public CustomerDashboardApplicationApproved(String securityCode, String loanAmount, String emi, String tenure,
			String loanContractNumber, String disbursementDate, String disbursementMethod, String applicationDate,
			String applicationStatus, String loanContractUrl) {
		super();
		this.securityCode = securityCode;
		this.loanAmount = loanAmount;
		this.emi = emi;
		this.tenure = tenure;
		this.loanContractNumber = loanContractNumber;
		this.disbursementDate = disbursementDate;
		this.disbursementMethod = disbursementMethod;
		this.applicationDate = applicationDate;
		this.applicationStatus = applicationStatus;
		this.loanContractUrl = loanContractUrl;
	}
	public CustomerDashboardApplicationApproved() {
		super();
		this.securityCode = "";
		this.loanAmount = "";
		this.emi = "";
		this.tenure = "";
		this.loanContractNumber = "";
		this.disbursementDate = "";
		this.disbursementMethod = "";
		this.applicationDate = "";
		this.applicationStatus = "";
	}
	public String getLoanContractNumber() {
		return loanContractNumber;
	}
	public void setLoanContractNumber(String loanContractNumber) {
		this.loanContractNumber = loanContractNumber;
	}
	public String getDisbursementDate() {
		return disbursementDate;
	}
	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}
	public String getDisbursementMethod() {
		return disbursementMethod;
	}
	public void setDisbursementMethod(String disbursementMethod) {
		this.disbursementMethod = disbursementMethod;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getEmi() {
		return emi;
	}
	public void setEmi(String emi) {
		this.emi = emi;
	}
	public String getTenure() {
		return tenure;
	}
	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	public String getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getLoanContractUrl() {
		return loanContractUrl;
	}
	public void setLoanContractUrl(String loanContractUrl) {
		this.loanContractUrl = loanContractUrl;
	}
}
