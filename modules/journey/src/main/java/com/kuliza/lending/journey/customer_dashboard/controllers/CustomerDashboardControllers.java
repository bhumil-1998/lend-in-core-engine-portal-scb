package com.kuliza.lending.journey.customer_dashboard.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.customer_dashboard.services.CustomerDashboardServices;

@RestController
@RequestMapping("/customer-dashboard")
public class CustomerDashboardControllers {

	@Autowired
	CustomerDashboardServices customerDashboardServices;

	private static final Logger logger = LoggerFactory.getLogger(CustomerDashboardControllers.class);

	@RequestMapping(method = RequestMethod.GET, value = "/get-dashboard-data")
	public ResponseEntity<Object> getDashboardData(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getDashboardData(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-applications")
	public ResponseEntity<Object> getApplications(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getApplicationsData(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-profile")
	public ResponseEntity<Object> getProfileData(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getProfileData(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-notifications")
	public ResponseEntity<Object> getNotifications(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getNotifications(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-repayment-schedule")
	public ResponseEntity<Object> getRepaymentSchedule(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getRepaymentSchedule(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-settings")
	public ResponseEntity<Object> getSettings(Principal principal) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(customerDashboardServices.getSettings(principal.getName()));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

}
