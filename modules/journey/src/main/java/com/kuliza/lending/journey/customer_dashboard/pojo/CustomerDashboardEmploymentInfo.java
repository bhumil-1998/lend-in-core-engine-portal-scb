package com.kuliza.lending.journey.customer_dashboard.pojo;

import java.util.Map;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Enums;
import com.kuliza.lending.journey.model.LOSUserEmploymentModel;

public class CustomerDashboardEmploymentInfo {

	private String employmentStatus;
	private String companyName;
	private String companyType;
	private String occupation;
	private String position;

	public CustomerDashboardEmploymentInfo() {
		super();
		this.employmentStatus = "";
		this.companyName = "";
		this.companyType = "";
		this.occupation = "";
		this.position = "";
	}

	public CustomerDashboardEmploymentInfo(String employmentStatus, String companyName, String companyType,
			String occupation, String position) {
		super();
		this.employmentStatus = employmentStatus;
		this.companyName = companyName;
		this.companyType = companyType;
		this.occupation = occupation;
		this.position = position;
	}

	public CustomerDashboardEmploymentInfo(Map<String, Object> data) {
		super();
		this.employmentStatus = CommonHelperFunctions.getStringValue(Enums.EmploymentStatus.SALARIED);
		this.companyName = CommonHelperFunctions.getStringValue(data.get(Constants.COMPANY_NAME_KEY));
		this.companyType = CommonHelperFunctions.getStringValue(data.get(Constants.COMPANY_TYPE_KEY));
		this.occupation = CommonHelperFunctions.getStringValue(data.get(Constants.OCCUPATION_KEY));
		this.position = CommonHelperFunctions.getStringValue(data.get(Constants.DESIGNATION_KEY));
	}

	public CustomerDashboardEmploymentInfo(LOSUserEmploymentModel losUserEmploymentModel) {
		super();
		if (losUserEmploymentModel != null) {
			this.employmentStatus = CommonHelperFunctions.getStringValue(losUserEmploymentModel.getEmploymentStatus());
			this.companyName = CommonHelperFunctions.getStringValue(losUserEmploymentModel.getCompanyName());
			this.companyType = CommonHelperFunctions.getStringValue(losUserEmploymentModel.getCompanyType());
			this.occupation = CommonHelperFunctions.getStringValue(losUserEmploymentModel.getOccupation());
			this.position = CommonHelperFunctions.getStringValue(losUserEmploymentModel.getPosition());
		} else {
			this.employmentStatus = "";
			this.companyName = "";
			this.companyType = "";
			this.occupation = "";
			this.position = "";
		}
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
